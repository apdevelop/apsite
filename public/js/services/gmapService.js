// public/js/services/aGmapService.js
angular.module('gmapService', ['uiGmapgoogle-maps'])
    .factory('gmapService', function($http, $rootScope, $q, $location) {

        var apiKey = 'AIzaSyARveAGxalTst0Rt99uS_QXpSwes8_k4Rw';
        // recives sections array and returns polylines

        var useSnapToRoad = false;

        var njCoords = [{
            lat: 40.947133,
            lng: -74.782274
        }, {
            lat: 40.934663,
            lng: -73.959783
        }, {
            lat: 40.464972,
            lng: -74.214202
        }, {
            lat: 40.496732,
            lng: -74.789071
        }];

        var getSectionsPolyLinesFunc = function(sections) {
            var deferred = $q.defer();

            var startMarkers = [];
            var routesBorder = [];
            var routes = [];

            for (i = 0; i < sections.length; i++) {

                var sectionDescriptionObj
                createSectionPolyLine(i, sections[i]).then(function(data) {
                    sectionDescriptionObj = data.routeDescriptionObj;
                    startMarkers.push(sectionDescriptionObj.startMarker);
                    routesBorder.push(sectionDescriptionObj.routeBorder);
                    routes.push(sectionDescriptionObj.route);

                });

            }

            deferred.resolve({
                startMarkers: startMarkers,
                routesBorder: routesBorder,
                routes: routes
            })

            return deferred.promise;
        }

        // 
        var getIncidentsMarkersFunc = function(incidents) {

            var deferred = $q.defer();

            var njBorders = new google.maps.Polygon({
                paths: njCoords
            })
            // check in incidents inside nj:

            var incidentsMarkers = [];

            for (i = 0; i < incidents.length; i++) {
                var incidentLoc = new google.maps.LatLng(
                    parseFloat(incidents[i].location.lat),
                    parseFloat(incidents[i].location.lng)
                );
                if (google.maps.geometry.poly.containsLocation(incidentLoc, njBorders)) {

                    switch (incidents[i].type) {
                        case 'ACCIDENT':
                            incidentsMarkers.push(createAccidentMarker(i, incidents[i]));
                            break;
                        case 'ROAD_INCIDENT':
                            incidentsMarkers.push(createRoadIncidentMarker(i, incidents[i]));
                            break;
                        case 'ROAD_RESTRICTION':
                            // TODO: create road restriction marker
                            break;
                        default:
                            console.log("ERROR, invalid incident type");
                    }
                }

                incidentsMarkers.push(createAccidentMarker(i, incidents[i]));
            }

            deferred.resolve({
                incidentsMarkers: incidentsMarkers
            })

            return deferred.promise;

        }


        function createAccidentMarker(id, accident) {

            var accidentResult;
            var injury = "";

            if (accident.optSection == "NONE") {
                accidentResult = "";
            } else if (accident.optAccidentResultType == 'ACCIDENT_PREDICTED') {
                accidentResult = "Predicted";
            } else {
                accidentResult = "NotPredicted";
            }

            if (accident.title.indexOf("Injuries") > -1) {
                injury = "Injury";
            }


            var icon = {
                url: "resources/images/accidents/Accident" + accidentResult + injury + ".png",
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            }

            var accidentMarker = ({
                id: [id],
                title: accident.title + "," + accident.crTime,
                latitude: accident.location.lat,
                longitude: accident.location.lng,
                optionsAccidents: {
                    icon: icon
                }

            })

            return accidentMarker;
        }


        function createRoadIncidentMarker(id, incident) {

            var icon = {
                url: "resources/images/accidents/roadBlock.png",
                scaledSize: new google.maps.Size(25, 25),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0)
            }

            var incidentMarker = ({
                id: [id],
                title: incident.title + "," + incident.crTime,
                latitude: incident.location.lat,
                longitude: incident.location.lng,
                optionsIncidents: {
                    icon: icon
                }

            })

            return incidentMarker;
        }

        function createSectionPolyLine(id, section) {
            var deferredSnap = $q.defer();

            var path = new google.maps.Polyline({
                path: section.locationDescription.routeDescription
            })


            var stateColor = '';
            switch (section.state.state) {
                case 'CLEAR':
                    stateColor = 'green';
                    break;
                case 'MEDIUM':
                    stateColor = 'yellow';
                    break;
                case 'HIGH':
                    stateColor = 'red';
            }

            var route;
            var routeBorder;
            var startmarker;

            if (useSnapToRoad) {

                $.get('https://roads.googleapis.com/v1/snapToRoads', {
                    interpolate: true,
                    key: apiKey,
                    path: pathValues.join('|')

                }, function(data) {

                    var pathValues = [];
                    for (var i = 0; i < path.getPath().getLength(); i++) {
                        pathValues.push(path.getPath().getAt(i).toUrlValue());
                    }

                    var snappedCoordinates = [];
                    var placeIdArray = [];
                    for (var i = 0; i < data.snappedPoints.length; i++) {
                        var latlng = new google.maps.LatLng(
                            data.snappedPoints[i].location.latitude,
                            data.snappedPoints[i].location.longitude);
                        snappedCoordinates.push(latlng);
                        placeIdArray.push(data.snappedPoints[i].placeId);
                    }

                    routeBorder = new google.maps.Polyline({
                        id: [10000 + i],
                        title: section.sectionName,
                        pathBorder: snappedCoordinates,
                        strokeBorder: {
                            color: '#4d4141',
                            weight: 4
                        }
                    });

                    route = new google.maps.Polyline({
                        id: [20000 + i],
                        title: section.sectionName,
                        pathRoute: snappedCoordinates,
                        strokeRoute: {
                            color: stateColor,
                            weight: 3
                        },
                        eventsRoute: {
                            dblclick: function() {
                                window.alert("DBL Click")
                            },
                            click: function() {
                                window.alert("Click")
                            }
                        }
                    });


                    startMarker = new google.maps.Marker({
                        id: [30000 + id], // routes id's 30000+ 
                        title: section.sectionName,
                        latitude: section.locationDescription.startLoc.lat,
                        longitude: section.locationDescription.startLoc.lng,
                        optionsStartMarkers: {
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 5,
                                fillOpacity: 0.9,
                                fillColor: '#666666',
                                strokeOpacity: 1,
                                strokeColor: '#4d4141',
                                strokeWeight: 2
                            }
                        },
                        events: {
                            click: function() {
                                console.log('click');
                            }
                        }
                    })

                    var routeDescriptionObj = {
                        routeBorder: routeBorder,
                        route: route,
                        startMarker: startMarker
                    };


                    deferredSnap.resolve({
                        routeDescriptionObj: routeDescriptionObj
                    });

                });
            } else {

                routeBorder = new google.maps.Polyline({
                    id: [10000 + i],
                    title: section.sectionName,
                    pathBorder: path.getPath().getArray(),
                    static: true,
                    strokeBorder: {
                        color: '#4d4141',
                        weight: 4
                    },
                    eventsRouteBorder: function(obj, eventName, model, args) {
                        var path = $location.path().split("/");
                        $location.path(path[0] + "/" + path[1] + "/section/" + model.title);
                    }

                });

                route = new google.maps.Polyline({
                    id: [20000 + i],
                    title: section.sectionName,
                    pathRoute: path.getPath().getArray(),
                    static: true,
                    strokeRoute: {
                        color: stateColor,
                        weight: 3
                    },
                    eventsRoute: {
                        click: function(obj, eventName, model, args) {
                            var path = $location.path().split("/");
                            $location.path(path[0] + "/" + path[1] + "/section/" + model.title);
                        }
                    }
                });


                startMarker = new google.maps.Marker({
                    id: [30000 + id], // routes id's 30000+ 
                    title: section.sectionName,
                    latitude: section.locationDescription.startLoc.lat,
                    longitude: section.locationDescription.startLoc.lng,
                    optionsStartMarkers: {
                        title: section.decoratedName + ", " + section.locationDescription.mile + ", " + section.locationDescription.length,
                        icon: {
                            path: google.maps.SymbolPath.CIRCLE,
                            scale: 5,
                            fillOpacity: 0.9,
                            fillColor: '#666666',
                            strokeOpacity: 1,
                            strokeColor: '#4d4141',
                            strokeWeight: 2
                        }
                    },
                    events: {
                        click: function(obj, eventName, model, args) {
                            var path = $location.path().split("/");
                            $location.path(path[0] + "/" + path[1] + "/section/" + model.title);
                        }
                    }
                })

                var routeDescriptionObj = {
                    routeBorder: routeBorder,
                    route: route,
                    startMarker: startMarker
                };


                deferredSnap.resolve({
                    routeDescriptionObj: routeDescriptionObj
                });


            }


            return deferredSnap.promise;

        }

        return {
            getSectionsPolyLines: getSectionsPolyLinesFunc,
            getIncidentsMarkers: getIncidentsMarkersFunc
        }
    });
