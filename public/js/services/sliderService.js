// public/js/services/sliderService.js
angular.module('sliderService', [])
    .factory('sliderService', function($q) {

    	var sliderValue = 0.5;

        var getSliderValueFunc = function() {
            var deferred = $q.defer();

            deferred.resolve({
            	sliderValue: sliderValue
            })

            return deferred.promise;
        }

    	var setSliderValueFunc = function(value){
    		console.log("SliderService: " + value)
    		sliderValue = value;
    	}

    	return {
    		setSliderValue: setSliderValueFunc,
    		getSliderValue: getSliderValueFunc
    	}

    });
