// public/js/services/incidentService.js
angular.module('incidentService',[])
	.factory('Incidents',['$http', function($http){
	
	var getAccidentsFunc = function(num){
		if (num > 0)
			return $http.get('/api/incidentsG/ACCIDENT/' + num);
		else
			return $http.get('/api/incidents/ACCIDENT');
	}	

	var getSingleRoadAccidentsFunc = function(section,num){
		if (num > 0)
			return $http.get('/api/incidents/ACCIDENT/' + section + '/' + num);
		else
			return $http.get('/api/incidents/ACCIDENT/' + section);
	}	

	var getRoadIncidentsFunc = function(num){
		if (num != null)
			return $http.get('/api/incidents_all/ROAD_INCIDENT/' + num);
		else
			return $http.get('/api/incidents_all/ROAD_INCIDENT');
	}	

	var getSingleRoadIncidentsFunc = function(section,num){
		if (num > 0)
			return $http.get('/api/incidents/ROAD_INCIDENT/' + section + '/'  + num);
		else
			return $http.get('/api/incidents/ROAD_INCIDENT/' + section);
	}


	var getAccidentsInRangeFunc = function(num){
		if (num > 0)
			return $http.get('/api/accidentsInRange/' + num);
		else
			return $http.get('/api/accidentsInRange');
	}
/*
TODO: Implement (last 24 hours for example)
	var getSingleRoadIncidentsWithTimeFunc = function(section, hours){
		return 
	}
	*/

	return{
		getAccidents : getAccidentsFunc,
		getSingleRoadAccidents : getSingleRoadAccidentsFunc,
		getRoadIncidents : getRoadIncidentsFunc,
		getSingleRoadIncidents : getSingleRoadIncidentsFunc,
		getAccidentsInRange: getAccidentsInRangeFunc
	}
}]);