// public/js/services/sectionService.js
angular.module('sectionService', [])
    .factory('Sections', ['$http', '$q', function($http, $q) {

        var initPromise = init();

        // Get all sections:
        var getOverviewFunc = function() {
            var deferred = $q.defer();

            initPromise.then(function(data) {
                deferred.resolve({
                    sections: data.sections
                })
            })

            return deferred.promise;
        };

        // get Single section:
        // Currentlly implementation of overview fetch all sections, 
        // use this to find single section without db call. in the future consider different implementation
        var getSectionFunc = function(sectionName) {

            var deferred = $q.defer();

            initPromise.then(function(data) {
                for (i = 0; i < data.sections.length; i++) {
                    if (sectionName === data.sections[i].sectionName) {
                        deferred.resolve({
                            section: data.sections[i]
                        })
                    }
                }
            })

            return deferred.promise;


        };


        function init() {
            var deferred = $q.defer();
            console.log("Server: Section Loading");

            $http.get('/api/sections')
                .success(function(data) {
                    deferred.resolve({
                        sections: data
                    })
                });
            return deferred.promise;
        }


        return {

            getOverview: getOverviewFunc,
            getSection: getSectionFunc

        }

    }]);
