// public/js/services/globalService
angular.module('globalService', [])
    .factory('Global', function() {


        // Convert section to DataTable format:
        function convertSectionFunc(section) {
            return {
                sectionId: section.sectionName,
                title: section.decoratedName,
                milePost: section.locationDescription.mile,
                length: section.locationDescription.length,
                type: section.sectionType,
                weatherConditions: convertWeatherConditions(section.weatherConditions),
                state: convertState(section.state)
            }
        }

        function convertState(state) {
            var stateObj;
            switch (state.state) {
                case 'CLEAR':
                    stateObj = {
                        description: 'Basic Risk',
                        FgColor: 'hsla(0, 0%, 0%, 1)',
                        BgColor: 'hsla(140, 90%, 30%, 0.6)'
                    }
                    break;
                case 'MEDIUM':
                    stateObj = {
                        description: 'Elevated Risk',
                        FgColor: 'hsla(0, 0%, 0%, 1)',
                        BgColor: 'hsla(55, 80%, 40%, 0.6)'
                    }
                    break;
                case 'HIGH':
                    stateObj = {
                        description: 'High Risk',
                        FgColor: 'hsla(0, 0%, 0%, 1)',
                        BgColor: 'hsla(360, 80%, 40%, 0.6)'
                    }
                    break;
                default:
                    console.err("Error, invalid state");
                    break;
            }
            return stateObj;
        }

        function convertWeatherConditions(weatherConditions) {
            var weatherConditionsObj;
            weatherConditionsObj = {
                description: weatherConditions.conditions.conditions
            }
            return weatherConditionsObj;
        }

        return{
        	convertSection : convertSectionFunc
        }


    });
