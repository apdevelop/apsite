// public/js/services/predictionService.js
angular.module('predictionService', [])
    .factory('Predictions', ['$http', '$q', function($http, $q) {

        var initPromise = init();

        var getAllPredictionsFunc = function() {

            var deferred = $q.defer();

            initPromise.then(function(data) {
                deferred.resolve({
                    predictions: data.predictions
                })
            })
            return deferred.promise;

        }

        var getParsedPredictionsFunc = function() {
            var deferred = $q.defer();

            initPromise.then(function(data) {
                var sectionsPrediction = [];
                for (i = 0; i < data.predictions.length; i++) {
                    sectionsPrediction[i] = [];
                    for (j = 0; j < data.predictions[i].prediction.length; j++) {
                        var sectionPredictionFull = data.predictions[i];
                        sectionsPrediction[i].push({
                            sectionName: sectionPredictionFull.sectionName,
                            decoratedName: sectionPredictionFull.decoratedName,
                            sectionType: sectionPredictionFull.sectionType,
                            locationDescription: sectionPredictionFull.locationDescription,
                            state: sectionPredictionFull.prediction[j].State,
                            weatherConditions: sectionPredictionFull.prediction[j].weatherConditions,
                            predictionTime: sectionPredictionFull.prediction[j].time,
                            predictionIndex: j
                        });
                    }
                }
                deferred.resolve({
                    sectionsPrediction: sectionsPrediction
                })
            })

            return deferred.promise;
        }


        var getSinglePredictionFunc = function(predictionIndex) {
            var deferred = $q.defer();
            initPromise.then(function(data) {

                var sectionsPrediction = []
                for (i = 0; i < data.predictions.length; i++) {
                    var sectionPredictionFull = data.predictions[i];
                    var sectionPrediction = {
                        sectionName: sectionPredictionFull.sectionName,
                        decoratedName: sectionPredictionFull.decoratedName,
                        sectionType: sectionPredictionFull.sectionType,
                        locationDescription: sectionPredictionFull.locationDescription,
                        state: sectionPredictionFull.prediction[predictionIndex].State,
                        weatherConditions: sectionPredictionFull.prediction[predictionIndex].weatherConditions,
                        predictionTime: sectionPredictionFull.prediction[predictionIndex].time,
                        predictionIndex: predictionIndex
                    }
                    sectionsPrediction.push(sectionPrediction);
                }

                deferred.resolve({
                    sectionsPrediction: sectionsPrediction
                })

            })

            return deferred.promise;

        }

        var getSectionPredictionFunc = function(sectionId) {
            var deferredSectionPrediction = $q.defer();
            console.log('/api/predictions/' + sectionId);
            $http.get('/api/predictions/' + sectionId)
                .success(function(data) {

                    // convert for section Prediction
                    console.log(data);
                    deferredSectionPrediction.resolve({
                        sectionPrediction: data[0]
                    })
                })

            return deferredSectionPrediction.promise;

        }

        function init() {
            var deferred = $q.defer();
            console.log("Server: Prediction loading");

            $http.get('/api/predictions')
                .success(function(data) {
                    deferred.resolve({
                        predictions: data
                    })
                });
            return deferred.promise;
        }


        return {
            getAllPredictions: getAllPredictionsFunc,
            getSinglePrediction: getSinglePredictionFunc,
            getParsedPredictions: getParsedPredictionsFunc,
            getSectionPrediction: getSectionPredictionFunc
        }


    }]);
