// public/js/app.js
var app = angular.module('wayCare', 
	['ui.router', 'ui.bootstrap', 'geolocation', 'datatables', 'datatables.bootstrap', 'angular-loading-bar', 'uiGmapgoogle-maps', 
			'ngAnimate', 'ds.clock', 'angularMoment', 'rzModule', 
	 'sectionService', 'incidentService', 'predictionService', 'sliderService', 'globalService', 'gmapService',
	 'headerCtrl', 'currentCtrl', 'predictionCtrl', 'decisionCtrl', 'overviewCtrl', 'sectionCtrl', 
	 'sectionPredictionCtrl', 'overviewPredictionCtrl', 'accidentsCtrl', 'gmapCtrl', 
	 'gmapPredictionCtrl', 'accidentsSummaryCtrl', 'accidentsLastCtrl']);

app.config(function($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider){

	$urlRouterProvider.otherwise('/current');

	$urlRouterProvider.when('/current','/current/overview');
	$urlRouterProvider.when('/prediction','/prediction/overview');

	$stateProvider
		
		// Current status main
		.state('current',{
			url: '/current',
			templateUrl: 'partials/current.html',
			controller: 'currentCtrl'
		})

		// nested views for current:
		.state('current.overview',{
			url:'/overview',
			templateUrl: 'partials/current-overview.html',
			controller: 'overviewCtrl'
		})

		.state('current.section',{
			url:'/section/:sectionName',
			templateUrl: 'partials/current-section.html',
			controller: 'sectionCtrl'
		})

		.state('current.accidents',{
			url:'/accidents',
			templateUrl: 'partials/current-accidents.html',
			controller: 'accidentsCtrl'
		})

		// prediction main
		.state('prediction', {
			url: '/prediction',
			templateUrl: 'partials/prediction.html',
			controller: 'predictionCtrl'

		})

		// nested views for prediction
		.state('prediction.overview',{
			url:'/overview',
			templateUrl: 'partials/prediction-overview.html',
			controller: 'overviewPredictionCtrl'
		})

		.state('prediction.section',{
			url:'/section/:sectionName',
			templateUrl: 'partials/prediction-section.html',
			controller: 'sectionPredictionCtrl'
		})

		// desiciton main:
		.state('decision', {
			url:'/decision',
			templateUrl: 'partials/decision.html',
			controller: 'decisionCtrl'
		})


		// accidents summery
		.state('accidents-summary',{
			url: '/accidents-summary',
			templateUrl: 'partials/accidents-summary.html',
			controller: 'accidentsSummaryCtrl'
		})

		// last accidents map
		.state('accidents-last',{
			url: '/accidents-last',
			templateUrl: 'partials/accidents-last.html',
			controller: 'accidentsLastCtrl'
		})



		uiGmapGoogleMapApiProvider.configure({
			key: 'AIzaSyARveAGxalTst0Rt99uS_QXpSwes8_k4Rw',
			v: '3.20',
			libraries: 'drawing,places,geometry'
		});

});


// Directives:
app.directive('animateOnChange', function($animate, $timeout){
	return function($scope, elem, attr){
		$scope.$watch('sliderChange', function(){
			var c = 'change';
			if($scope.sliderChange){
				$animate.addClass(elem,c);
			} else {
				$animate.removeClass(elem,c);
			}
		})
	}
})