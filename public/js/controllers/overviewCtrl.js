// public/js/controllers/overviewCtrl.js
angular.module('overviewCtrl', ['datatables', 'datatables.bootstrap'])

.controller('overviewCtrl', [
    '$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$location',
    'Sections', 'Global',
    function($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, $location, Sections, Global) {
        $scope.loading = true;

        $scope.changeView = function(url) {
            console.log('/current/section/' + url);
            $location.path('/current/section/' + url);
        }

        Sections.getOverview().then(function(data) {
            // Convert sections for datatables
            var sectionsDataTable = [];
            for (i = 0; i < data.sections.length; i++) {
                sectionsDataTable.push(Global.convertSection(data.sections[i]))
            }
            $scope.sections = sectionsDataTable;

        })

        // Table
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('simple_numbers')
            .withDisplayLength(10)
            .withBootstrap()
            .withOption('bFilter', true)
            .withOption('lengthMenu', [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]);

        vm.dtColumnDefs = [];

        $scope.dtOptions = vm.dtOptions;
        $scope.dtColumnDefs = vm.dtColumnDefs;

    }
])
