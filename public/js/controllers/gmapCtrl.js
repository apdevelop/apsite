// public/js/controllers/aGmapCtrl.js
angular.module('gmapCtrl', [])
    .controller('gmapCtrl', ['$scope', 'gmapService', 'Sections', 'Incidents', 'uiGmapGoogleMapApi',
        function($scope, gmapService, Sections, Incidents, uiGmapGoogleMapApi) {

            $scope.startMarkers = [];
            $scope.routesBorder = [];
            $scope.routes = [];

            $scope.map = {
                center: {
                    latitude: 32.073847,
                    longitude: 34.795831
                },
                zoom: 13
            };

            uiGmapGoogleMapApi.then(function(maps) {

                Incidents.getAccidents(70)
                    .success(function(data) {
                        gmapService.getIncidentsMarkers(data).then(function(promise) {
                            $scope.accidentsMarker = promise.incidentsMarkers;
                        })

                    })

                Incidents.getRoadIncidents(10)
                    .success(function(data){
                        gmapService.getIncidentsMarkers(data).then(function(promise) {
                            $scope.incidentsMarker = promise.incidentsMarkers;
                        })
                    })

                Sections.getOverview().then(function(data) {
                    gmapService.getSectionsPolyLines(data.sections).then(function(promise) {
                        $scope.startMarkers = promise.startMarkers;
                        $scope.routesBorder = promise.routesBorder;
                        $scope.routes = promise.routes;
                    })
                })
            });

        }
    ]);
