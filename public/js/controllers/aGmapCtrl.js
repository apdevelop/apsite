// public/js/controllers/aGmapCtrl.js
angular.module('aGmapCtrl', [])
    .controller('aGmapCtrl', ['$scope', 'aGmapService', 'Sections', 'Incidents', 'uiGmapGoogleMapApi',
        function($scope, aMapService, Sections, Incidents, uiGmapGoogleMapApi) {

            $scope.startMarkers = [];
            $scope.routesBorder = [];
            $scope.routes = [];

            $scope.map = {
                center: {
                    latitude: 40.720020,
                    longitude: -74.379976
                },
                zoom: 11
            };

            uiGmapGoogleMapApi.then(function(maps) {

                Incidents.getAccidents(10)
                    .success(function(data) {
                        aMapService.getIncidentsMarkers(data).then(function(promise) {
                            $scope.accidentsMarker = promise.incidentsMarkers;
                        })

                    })

                Sections.getOverview().then(function(data) {
                    aMapService.getSectionsPolyLines(data.sections).then(function(promise) {
                        $scope.startMarkers = promise.startMarkers;
                        $scope.routesBorder = promise.routesBorder;
                        $scope.routes = promise.routes;
                    })
                })
            });

        }
    ]);
