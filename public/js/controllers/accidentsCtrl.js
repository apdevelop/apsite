// public/js/controllers/accidentCtrl.js
angular.module('accidentsCtrl',['datatables', 'datatables.bootstrap'])

	.controller('accidentsCtrl', ['$scope', '$http', 'Incidents', 'DTOptionsBuilder', 'DTColumnDefBuilder', 
		function($scope, $http, Incidents, DTOptionsBuilder, DTColumnDefBuilder){

		$scope.loading = true;
		Incidents.getAccidents(15)
			.success(function(data){
				$scope.incidents = data;
				$scope.loading = false;
			});


		$scope.idSelectedAccident = null;

        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('simple_numbers')
            .withDisplayLength(5)
            .withBootstrap()
            .withOption('bFilter', true)
            .withOption('lengthMenu', [
                [5, 10, -1],
                [5, 10, "All"]
            ]);

        vm.dtColumnDefs = [];

        $scope.dtOptions = vm.dtOptions;
        $scope.dtColumnDefs = vm.dtColumnDefs;
}])