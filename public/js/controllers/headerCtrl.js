// public/js/controllers/headerCtrl.js
var headerCtrl = angular.module('headerCtrl', ['angularMoment']);
headerCtrl.controller('headerCtrl', ['$scope', '$rootScope', '$location', 'sliderService', 
    function($scope, $rootScope, $location, sliderService) {


    $rootScope.skin = 'skin-blue';

    $scope.time = moment();
    $scope.gmtValue = 2;
    $scope.slider_ticks_values = 0.5;

    $scope.isActive = function(viewLocation) {
        if ($location.path().indexOf('/prediction') > -1) {
            $scope.clock = false;
            $scope.predictionClock = true;
            $scope.predictionSlider = true;
            $rootScope.skin = 'skin-purple';
            $scope.gmtValue = 2 + 0.7;
        } else {
            $scope.clock = true;
            $scope.predictionClock = false;
            $scope.predictionSlider = false;
            $rootScope.skin = 'skin-blue';
            $scope.gmtValue = 2;

        }
        return ($location.path().indexOf(viewLocation) > -1);
    }

    //Slider with ticks and values
    $scope.prediction_slider = {
        value: 0,
        options: {
            ceil: 47,
            floor: 0,
            step: 1,
            precision: 1,
            hideLimitLabels: true,
            showSelectionBar: true,
            getSelectionBarColor: function(value) {
                if (value <= 15)
                    return 'green';
                if (value <= 30)
                    return 'orange';
                if (value <= 48)
                    return 'red';
                return '#2AE02A';
            },
            onChange: function() {
                $scope.sliderChange = true;
                $rootScope.sliderValueOnChange = $scope.prediction_slider.value;
            },
            onEnd: function() {
                $scope.sliderChange = false;
                $rootScope.sliderValue = $scope.prediction_slider.value;
            }

        }
    };


    $scope.$watch('prediction_slider.value', function() {
        var sliderTime = getTime($scope.prediction_slider.value);
        var predictionTime = moment().add(sliderTime.nextDay, 'd');
        predictionTime.set('hour', sliderTime.hour + 7);
        predictionTime.set('minute', sliderTime.minute);
        predictionTime.set('second', 0);
        $scope.predictionTime = predictionTime.toDate();
    })


    $scope.clickHeaderTab = function(location) {
        switch (location) {
            case 'current':
                $scope.predictionSlider = false;
                $rootScope.skin = 'skin-blue';
                $scope.gmtValue = 3;
                break;
            case 'prediction':
                $scope.predictionSlider = true;
                $rootScope.skin = 'skin-purple';
                $scope.gmtValue = 3 + 0.7;
                break;
            default:
                $rootScope.skin = 'skin-blue';
                $scope.gmtValue = 3;
                break;
        }
    }

    getStartIndex();

    function getStartIndex() {
        
        $rootScope.doneInitPredictionIndex = false;
        // calculate start index (current time + half an hour)
        var now = moment();
        var nowUtc = moment.utc();
        var mnUtc = moment.utc().hour(5).minute(0).second(0);

        var diffMinutes = (nowUtc - mnUtc)/1000/60;
        var startIndex = Math.ceil(diffMinutes/30);

        $rootScope.predictionStartIndex = startIndex;
        $rootScope.doneInitPredictionIndex = true;

    }



    function getTime(value) {


        var addedMinutes = ($rootScope.predictionStartIndex + value)*30;
        var mnUtc = moment.utc().hour(5).minute(0).second(0);
        var nvUtc = mnUtc.add(addedMinutes,'m');

        var nextDay = 0;
        if (addedMinutes >= 1440)
            nextDay = 1;

        var hour = nvUtc.hour() - 5;
        if (hour < 0)
            hour+=24;
        var minutes = nvUtc.minute();

        var sliderString = hour + ":" + ( (minutes < 30) ? "00" : "30");

        return {
            hour: hour,
            minute: minutes,
            nextDay: nextDay,
            sliderString: sliderString
        }
    }


}])
