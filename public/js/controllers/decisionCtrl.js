// public/js/controllers/decisionCtrl.js
var decisionCtrl = angular.module('decisionCtrl',[]);
decisionCtrl.controller('decisionCtrl',[ '$scope', '$location', function($scope, $location){

		$scope.isActive = function(viewLocation){
			
			var result = ($location.path().indexOf(viewLocation) > -1);
			return result;
		}

		// add controller logic for decision making here

}]);