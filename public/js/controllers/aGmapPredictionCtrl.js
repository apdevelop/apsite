// public/js/controllers/aGmapPredictionCtrl.js
angular.module('aGmapPredictionCtrl', [])
    .controller('aGmapPredictionCtrl', ['$scope', '$rootScope', '$q', 'aGmapService', 'Predictions', 'Incidents', 'uiGmapGoogleMapApi', 'sliderService', 'Global',
        function($scope, $rootScope, $q, aGMapService, Predictions, Incidents, uiGmapGoogleMapApi, sliderService, Global) {

            console.log("inside predictionCtrl");
            $scope.startMarkers = [];
            $scope.routesBorder = [];
            $scope.routes = [];

            $scope.map = {
                center: {
                    latitude: 40.720020,
                    longitude: -74.379976
                },
                zoom: 11
            };

            // Init controller:
            // store all sections prediction in dataBase
            var sectionsPrediction;

            // store each prediction in data base, each time sliderValue changes just get the index from data base and draw map
            $scope.doneInit = false;
            var init = function() {
                Predictions.getParsedPredictions().then(function(data) {
                    sectionsPrediction = data.sectionsPrediction;
                    $scope.doneInit = true;
                })
            }

            init();


            // init map (store first prediction only)
            $scope.$watch('doneInit', function() {
                if ($scope.doneInit) {
                    uiGmapGoogleMapApi.then(function(maps) {
                        // initialize sections on map
                        console.log($rootScope.predictionStartIndex)
                        Predictions.getSinglePrediction($rootScope.predictionStartIndex).then(function(data) {
                            aGMapService.getSectionsPolyLines(data.sectionsPrediction).then(function(promise) {
                                $scope.startMarkers = promise.startMarkers;
                                $scope.routesBorder = promise.routesBorder;
                                $scope.routes = promise.routes;
                            })
                        })

                    });
                }
            });

            // sliderValue change
            $rootScope.$watch('sliderValueOnChange', function() {
                var sliderValue = $rootScope.sliderValueOnChange;
                var predictionIndex = $rootScope.predictionStartIndex + $rootScope.sliderValueOnChange;
                console.log('predictionIndex ' + predictionIndex);
                if ($scope.routes != null) {
                    for (i = 0; i < $scope.routes.length; i++) {
                        var stateColor = '';
                        switch (sectionsPrediction[i][predictionIndex].state.state) {
                            case 'CLEAR':
                                stateColor = 'green';
                                break;
                            case 'MEDIUM':
                                stateColor = 'yellow';
                                break;
                            case 'HIGH':
                                stateColor = 'red';
                        }
                        $scope.routes[i].strokeRoute.color = stateColor;
                    }
                }
            })





        }
    ]);
