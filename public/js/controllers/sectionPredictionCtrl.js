// public/js/controllers/sectionPredictionCtrl.js
var sectionCtrl = angular.module('sectionPredictionCtrl', ['datatables', 'datatables.bootstrap', 'highcharts-ng']);
sectionCtrl.controller('sectionPredictionCtrl', ['$scope', '$rootScope', '$location', '$stateParams', 'DTOptionsBuilder', 'DTColumnDefBuilder',
    'Predictions',
    function($scope, $rootScope, $location, $stateParams, DTOptionsBuilder, DTColumnDefBuilder,
        Predictions) {

        // init:
        var sectionPrediction;
        var sliderValue = 1;
        $scope.sectionPredictionLoad = true;
        Predictions.getSectionPrediction($stateParams.sectionName).then(function(data) {
            sectionPrediction = data.sectionPrediction;
            $scope.sectionPredictionLoad = false;
        })

        $scope.$watch('sectionPredictionLoad', function() {
            if ($scope.sectionPredictionLoad == false) {

                sliderValue = ($rootScope.sliderValue != null) ? $rootScope.sliderValue : sliderValue;
                var section = {};
                var sectionObj = sectionPrediction;
                // create current section object:

                // sectionName
                // section start milePost
                // section length
                // section type
                // weather condition
                // current incidents (last 2 hours)
                // last accidents with status

                section.name = sectionObj.decoratedName;
                section.image = (sectionObj.sectionName === 'gsp_145') ? 'city_hw1.png' : 'hw1.jpg';

                section.mile = sectionObj.locationDescription.mile;
                section.length = sectionObj.locationDescription.length;
                section.type = sectionObj.sectionType.replace('_', ' ');

                switch (sectionObj.prediction[sliderValue].State.state) {
                    case 'CLEAR':
                        section.state = 'Basic Risk';
                        section.headerBgColor = 'bg-green'
                        section.alert = {
                            bgColor: 'bg-green',
                            alertIcon: ''
                        }
                        break;
                    case 'MEDIUM':
                        section.state = 'Elevetaed Risk';
                        section.headerBgColor = 'bg-yellow'
                        section.alert = {
                            bgColor: 'bg-yellow',
                            alertIcon: 'fa-lightbulb-o'
                        }
                        break;
                    case 'HIGH':
                        section.state = 'High Risk';
                        section.headerBgColor = 'bg-red'
                        section.alert = {
                            bgColor: 'bg-red',
                            alertIcon: 'fa-exclamation'
                        }
                        break;
                }

                // parse weather condition:
                var wcJson = sectionObj.prediction[sliderValue].weatherConditions.conditions;
                var conditions = [];

                // rain
                switch (wcJson.rain) {
                    case -1:
                        break;
                    case 0:
                        break;
                    case 1:
                        conditions.push({
                            condition: "Light rain",
                            alertBgColor: 'bg-yellow'
                        })
                        break;
                    case 2:
                        conditions.push({
                            condition: "Moderate rain",
                            alertBgColor: 'bg-orange'
                        })
                        break;
                    case 3:
                        conditions.push({
                            condition: "Heavy rain",
                            alertBgColor: 'bg-red'
                        })
                        break;
                    default:
                }

                // snow
                switch (wcJson.snow) {
                    case -1:
                        break;
                    case 0:
                        break;
                    case 1:
                        conditions.push({
                            condition: "Light snow",
                            alertBgColor: 'bg-yellow'
                        })
                        break;
                    case 2:
                        conditions.push({
                            condition: "Moderate snow",
                            alertBgColor: 'bg-orange'
                        })
                        break;
                    case 3:
                        conditions.push({
                            condition: "Heavy snow",
                            alertBgColor: 'bg-red'
                        })
                        break;
                    default:
                }

                // fog
                switch (wcJson.fog) {
                    case -1:
                        break;
                    case 0:
                        break;
                    case 1:
                        conditions.push({
                            condition: "Light fog",
                            alertBgColor: 'bg-yellow'
                        })
                        break;
                    case 2:
                        conditions.push({
                            condition: "Moderate fog",
                            alertBgColor: 'bg-orange'
                        })
                        break;
                    case 3:
                        conditions.push({
                            condition: "Heavy fog",
                            alertBgColor: 'bg-red'
                        })
                        break;
                    default:
                }

                // wind
                if (wcJson.windSpeed > 10) {
                    conditions.push({
                        condition: "Windy",
                        alertBgColor: 'bg-orange'
                    })
                }

                var weatherConditions = {
                    visibility: wcJson.visibility,
                    temperature: wcJson.temperature,
                    description: wcJson.conditions,
                    conditions: conditions
                }

                section.weatherConditions = weatherConditions;

                $scope.section = section;

            }
        })

        $rootScope.$watch('sliderValue', function() {
            sliderValue = ($rootScope.sliderValue != null) ? $rootScope.sliderValue : sliderValue;

            var section = {};
            var sectionObj = sectionPrediction;
            console.log(sectionPrediction);
            console.log(sectionObj);
            console.log(sliderValue);

            // create current section object:

            // sectionName
            // section start milePost
            // section length
            // section type
            // weather condition
            // current incidents (last 2 hours)
            // last accidents with status

            // TODO: add additional database object for decoration section
            section.name = sectionObj.decoratedName;

            section.image = (sectionObj.sectionName === 'gsp_145') ? 'city_hw1.png' : 'hw1.jpg';

            section.mile = sectionObj.locationDescription.mile;
            section.length = sectionObj.locationDescription.length;
            section.type = sectionObj.sectionType.replace('_', ' ');

            console.log(sectionObj.prediction[sliderValue]);
            switch (sectionObj.prediction[sliderValue].State.state) {
                case 'CLEAR':
                    section.state = 'Basic Risk';
                    section.headerBgColor = 'bg-green'
                    section.alert = {
                        bgColor: 'bg-green',
                        alertIcon: ''
                    }
                    break;
                case 'MEDIUM':
                    section.state = 'Elevetaed Risk';
                    section.headerBgColor = 'bg-yellow'
                    section.alert = {
                        bgColor: 'bg-yellow',
                        alertIcon: 'fa-lightbulb-o'
                    }
                    break;
                case 'HIGH':
                    section.state = 'High Risk';
                    section.headerBgColor = 'bg-red'
                    section.alert = {
                        bgColor: 'bg-red',
                        alertIcon: 'fa-exclamation'
                    }
                    break;
            }

            // parse weather condition:
            var wcJson = sectionObj.prediction[sliderValue].weatherConditions.conditions;
            var conditions = [];

            // rain
            switch (wcJson.rain) {
                case -1:
                    break;
                case 0:
                    break;
                case 1:
                    conditions.push({
                        condition: "Light rain",
                        alertBgColor: 'bg-yellow'
                    })
                    break;
                case 2:
                    conditions.push({
                        condition: "Moderate rain",
                        alertBgColor: 'bg-orange'
                    })
                    break;
                case 3:
                    conditions.push({
                        condition: "Heavy rain",
                        alertBgColor: 'bg-red'
                    })
                    break;
                default:
            }

            // snow
            switch (wcJson.snow) {
                case -1:
                    break;
                case 0:
                    break;
                case 1:
                    conditions.push({
                        condition: "Light snow",
                        alertBgColor: 'bg-yellow'
                    })
                    break;
                case 2:
                    conditions.push({
                        condition: "Moderate snow",
                        alertBgColor: 'bg-orange'
                    })
                    break;
                case 3:
                    conditions.push({
                        condition: "Heavy snow",
                        alertBgColor: 'bg-red'
                    })
                    break;
                default:
            }

            // fog
            switch (wcJson.fog) {
                case -1:
                    break;
                case 0:
                    break;
                case 1:
                    conditions.push({
                        condition: "Light fog",
                        alertBgColor: 'bg-yellow'
                    })
                    break;
                case 2:
                    conditions.push({
                        condition: "Moderate fog",
                        alertBgColor: 'bg-orange'
                    })
                    break;
                case 3:
                    conditions.push({
                        condition: "Heavy fog",
                        alertBgColor: 'bg-red'
                    })
                    break;
                default:
            }

            // wind
            if (wcJson.windSpeed > 10) {
                conditions.push({
                    condition: "Windy",
                    alertBgColor: 'bg-orange'
                })
            }

            var weatherConditions = {
                visibility: wcJson.visibility,
                temperature: wcJson.temperature,
                description: wcJson.conditions,
                conditions: conditions
            }

            section.weatherConditions = weatherConditions;

            $scope.section = section;


        });



    }
]);
