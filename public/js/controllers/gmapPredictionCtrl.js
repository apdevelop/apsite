// public/js/controllers/aGmapPredictionCtrl.js
angular.module('gmapPredictionCtrl', [])
    .controller('gmapPredictionCtrl', ['$scope', '$rootScope', '$q', 'gmapService', 'Predictions', 'Incidents', 'uiGmapGoogleMapApi', 'sliderService', 'Global',
        function($scope, $rootScope, $q, gmapService, Predictions, Incidents, uiGmapGoogleMapApi, sliderService, Global) {

            $scope.startMarkers = [];
            $scope.routesBorder = [];
            $scope.routes = [];

            $scope.map = {
                center: {
                    latitude: 32.073847,
                    longitude: 34.795831
                },
                zoom: 13
            };

            // Init controller:
            // store all sections prediction in dataBase
            var sectionsPrediction;

            // store each prediction in data base, each time sliderValue changes just get the index from data base and draw map
            $scope.doneInit = false;
            var init = function() {
                Predictions.getParsedPredictions().then(function(data) {
                    sectionsPrediction = data.sectionsPrediction;
                    $scope.doneInit = true;
                })
            }

            init();


            // init map (store first prediction only)
            $scope.$watch('doneInit', function() {
                if ($scope.doneInit) {
                    uiGmapGoogleMapApi.then(function(maps) {
                        // initialize sections on map
                        Predictions.getSinglePrediction($rootScope.predictionStartIndex).then(function(data) {
                            gmapService.getSectionsPolyLines(data.sectionsPrediction).then(function(promise) {
                                $scope.startMarkers = promise.startMarkers;
                                $scope.routesBorder = promise.routesBorder;
                                $scope.routes = promise.routes;
                            })
                        })

                    });
                }
            });

            // sliderValue change
            $rootScope.$watch('sliderValueOnChange', function() {
                var predictionIndex = $rootScope.sliderValueOnChange + $rootScope.predictionStartIndex;
                if ($scope.routes != null) {
                    for (i = 0; i < $scope.routes.length; i++) {
                        var stateColor = '';
                        switch (sectionsPrediction[i][predictionIndex].state.state) {
                            case 'CLEAR':
                                stateColor = 'green';
                                break;
                            case 'MEDIUM':
                                stateColor = 'yellow';
                                break;
                            case 'HIGH':
                                stateColor = 'red';
                        }
                        $scope.routes[i].strokeRoute.color = stateColor;
                    }
                }
            })





        }
    ]);
