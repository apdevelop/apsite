// public/js/controllers/overviewPredictionCtrl.js
angular.module('overviewPredictionCtrl', ['datatables', 'datatables.bootstrap'])

.controller('overviewPredictionCtrl', [ '$scope', '$rootScope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', '$location',
    'Predictions', 'sliderService', 'Global',
    function($scope, $rootScope, $http, DTOptionsBuilder, DTColumnDefBuilder, $location, Predictions, sliderService, Global) {

        $scope.loading = true;

    	var initIndex = 0;
        var sliderValue = 1;
        var predictionStartIndex = 0;


    	$rootScope.$watch('doneInitPredictionIndex', function(){
            if ($rootScope.doneInitPredictionIndex){
                predictionStartIndex = $rootScope.predictionStartIndex;
            }
        })

        $rootScope.$watch('sliderValue', function() {
            sliderValue = ($rootScope.sliderValue != null) ? $rootScope.sliderValue : sliderValue ;
            Predictions.getSinglePrediction(predictionStartIndex + sliderValue).then(function(data) {
                var sections = [];
                for (i=0; i < data.sectionsPrediction.length; i++){

                    sections.push(Global.convertSection(data.sectionsPrediction[i]));
                    
                }

                var highFlagSB = false;
                for(i=0; i < sections.length; i++){
                    if (sections[i].sectionId.indexOf('SB') > -1){
                        console.log(sections[i]);
                        if ( sections[i].state.description == 'High Risk' && highFlagSB == true){
                            sections[i].state.description = 'Elevated Risk';
                            sections[i].state.FgColor = 'hsla(0, 0%, 0%, 1)';
                            sections[i].state.BgColor = 'hsla(55, 80%, 40%, 0.6)';
                        } else if (sections[i].state.description === 'High Risk'){
                            highFlagSB = true;
                        }
                    }

                }

                var highFlagNB = false;
                for(i=0; i < sections.length; i++){
                    if (sections[i].sectionId.indexOf('NB') > -1){
                        console.log(sections[i]);
                        if ( sections[i].state.description == 'High Risk' && highFlagNB == true){
                            sections[i].state.description = 'Elevated Risk';
                            sections[i].state.FgColor = 'hsla(0, 0%, 0%, 1)';
                            sections[i].state.BgColor = 'hsla(55, 80%, 40%, 0.6)';
                        } else if (sections[i].state.description === 'High Risk'){
                            highFlagNB = true;
                        }
                    }

                }

                $scope.sections = sections;
            
            })
        });

        $scope.changeView = function(url) {
            $location.path('/prediction/section/' + url);
        }

        // Table
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('simple_numbers')
            .withDisplayLength(10)
            .withBootstrap()
            .withOption('bFilter', true)
            .withOption('lengthMenu', [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ]);

        vm.dtColumnDefs = [];

        $scope.dtOptions = vm.dtOptions;
        $scope.dtColumnDefs = vm.dtColumnDefs;
    }
]);
