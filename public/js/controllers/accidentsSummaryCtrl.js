// public/js/controllers/accidentsSummeryCtrl.js
var accidentsSummaryCtrl = angular.module('accidentsSummaryCtrl', ['datatables', 'datatables.bootstrap', ]);
accidentsSummaryCtrl.controller('accidentsSummaryCtrl', ['$scope', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Incidents',
    function($scope, DTOptionsBuilder, DTColumnDefBuilder, Incidents) {

        // Configure datatable
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('simple_numbers')
            .withDisplayLength(10)
            .withBootstrap()
            .withOption('bFilter', true)
            .withOption('lengthMenu', [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ]);

        vm.dtColumnDefs = [];

        $scope.dtOptions = vm.dtOptions;
        $scope.dtColumnDefs = vm.dtColumnDefs;

        Incidents.getAccidentsInRange(-1)
            .success(function(accidents) {
                $scope.totalAccidents = accidents.length;
                console.log('done loading all accidents');
                console.log(accidents);
                var predictedAccidents = 0;
                var notPredictedAccidents = 0;
                var accidentsArr = [];
                for (i = 0; i < accidents.length; i++) {
                    if (accidents[i].optAccidentResultType === 'ACCIDENT_PREDICTED') {
                        predictedAccidents++;
                        accidentsArr.push({
                            time: accidents[i].incidentTime,
                            title: accidents[i].title,
                            predicted: 'fa-check-circle text-success'
                        })
                    }
                    if (accidents[i].optAccidentResultType === 'ACCIDENT_NOT_PREDICTED') {
                        notPredictedAccidents++;
                        accidentsArr.push({
                            time: accidents[i].incidentTime,
                            title: accidents[i].title,
                            predicted: 'fa-times text-danger'
                        })
                    }
                }
                $scope.predictedAccidents = predictedAccidents;
                $scope.accidents = accidentsArr;
                $scope.successRate = (100.0 * (predictedAccidents / $scope.totalAccidents)).toFixed(2);
            })


    }
])
