// public/js/controllers/predictionCtrl.js
var predictionCtrl = angular.module('predictionCtrl', []);
predictionCtrl.controller('predictionCtrl', ['$scope', '$rootScope', '$location', 'Predictions',
    function($scope, $rootScope, $location, Predictions) {

        $scope.isActive = function(viewLocation) {

            var result = ($location.path().indexOf(viewLocation) > -1);
            return result;
        }

        // ------------------------------ //
        // -- Section dataBase handler -- //
        // ------------------------------ //
        Predictions.getAllPredictions().then(function(data) {
            $scope.predictions = data.predictions;
        })

        $scope.predictionLogo = 'logoBasicESm.png';
        $rootScope.$watch('sliderValue', function() {
            if ($rootScope.sliderValue != null) {
                if ($rootScope.sliderValue < 12) {
                    $scope.predictionLogo = 'logoBasicESm.png';
                } else if ( $rootScope.sliderValue< 24) {
                    $scope.predictionLogo = 'logoBasic1ESm.png';
                } else if ($rootScope.sliderValue < 36) {
                    $scope.predictionLogo = 'logoBasic2ESm.png';
                } else {
                    $scope.predictionLogo = 'logoBasic3ESm.png';
                }
            }

        });

    }
]);
