// public/js/controllers/sectionCtrl.js
var sectionCtrl = angular.module('sectionCtrl', ['datatables', 'datatables.bootstrap', 'highcharts-ng']);
sectionCtrl.controller('sectionCtrl', [
    '$scope', '$location', '$stateParams', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Sections', 'Incidents',
    function($scope, $location, $stateParams, DTOptionsBuilder, DTColumnDefBuilder, Sections, Incidents) {

        $scope.loading = true;

        $scope.isActive = function(viewLocation) {
                return ($location.path().indexOf(viewLocation) > -1);
            }
            // -------------------- //
            // -- Configure page -- //
            // -------------------- //

        // Configure datatable
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions()
            .withPaginationType('simple_numbers')
            .withDisplayLength(5)
            .withBootstrap()
            .withOption('bFilter', true)
            .withOption('lengthMenu', [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ]);

        vm.dtColumnDefs = [];

        $scope.dtOptions = vm.dtOptions;
        $scope.dtColumnDefs = vm.dtColumnDefs;


        // configure score gauge
        $scope.chartConfig = {
            options: {
                chart: {
                    type: 'solidgauge'
                },
                pane: {
                    center: ['50%', '80%'],
                    size: '150%',
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },
                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: -15,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }

                },
                tooltip: {
                    enabled: false
                },
                title: null
            },

            series: [],
            yAxis: {
                currentMin: 0,
                currentMax: 100,
                title: {
                    y: 0
                },
                stops: [
                    [0.5, '#DF5353'], // red
                    [0.7, '#DDDF0D'], // yellow
                    [0.8, '#55BF3B'] // green
                ],
                lineWidth: 0,
                tickInterval: 20,
                tickPixelInterval: 400,
                tickWidth: 0,
                labels: {
                    y: 15
                }
            },
            loading: $scope.loading
        }

        // ----------------------- //
        // -- Load section Data -- //
        // ----------------------- //
        Sections.getSection($stateParams.sectionName)
            .then(function(data) {

                var section = {};
                var sectionObj = data.section;
                // create current section object:

                // sectionName
                // section start milePost
                // section length
                // section type
                // weather condition
                // current incidents (last 2 hours)
                // last accidents with status

                // TODO: add additional database object for decoration section
                section.name = sectionObj.decoratedName;
                
                section.image = (sectionObj.sectionName === 'gsp_145') ? 'city_hw1.png' : 'hw1.jpg';

                section.mile = sectionObj.locationDescription.mile;
                section.length = sectionObj.locationDescription.length;
                section.type = sectionObj.sectionType.replace('_', ' ');

                switch (sectionObj.state.state) {
                    case 'CLEAR':
                        section.state = 'Basic Risk';
                        section.headerBgColor = 'bg-green'
                        section.alert = {
                            bgColor: 'bg-green',
                            alertIcon: ''
                        }
                        break;
                    case 'MEDIUM':
                        section.state = 'Elevetaed Risk';
                        section.headerBgColor = 'bg-yellow'
                        section.alert = {
                            bgColor: 'bg-yellow',
                            alertIcon: 'fa-lightbulb-o'
                        }
                        break;
                    case 'HIGH':
                        section.state = 'High Risk';
                        section.headerBgColor = 'bg-red'
                        section.alert = {
                            bgColor: 'bg-red',
                            alertIcon: 'fa-exclamation'
                        }
                        break;
                }

                // parse weather condition:
                var wcJson = sectionObj.weatherConditions.conditions;
                var conditions = [];

                // rain
                switch (wcJson.rain){
                case -1: 
                    break;
                case 0:
                    break;
                case 1:
                    conditions.push({
                        condition: "Light rain",
                        alertBgColor: 'bg-yellow'
                    })
                    break;
                case 2:
                    conditions.push({
                        condition: "Moderate rain",
                        alertBgColor: 'bg-orange'
                    })
                    break;
                case 3:
                    conditions.push({
                        condition: "Heavy rain",
                        alertBgColor: 'bg-red'
                    })
                    break;
                default:
                }

                // snow
                switch (wcJson.snow){
                case -1: 
                    break;
                case 0:
                    break;
                case 1:
                    conditions.push({
                        condition: "Light snow",
                        alertBgColor: 'bg-yellow'
                    })
                    break;
                case 2:
                    conditions.push({
                        condition: "Moderate snow",
                        alertBgColor: 'bg-orange'
                    })
                    break;
                case 3:
                    conditions.push({
                        condition: "Heavy snow",
                        alertBgColor: 'bg-red'
                    })
                    break;
                default:
                }

                // fog
                switch (wcJson.fog){
                case -1: 
                    break;
                case 0:
                    break;
                case 1:
                    conditions.push({
                        condition: "Light fog",
                        alertBgColor: 'bg-yellow'
                    })
                    break;
                case 2:
                    conditions.push({
                        condition: "Moderate fog",
                        alertBgColor: 'bg-orange'
                    })
                    break;
                case 3:
                    conditions.push({
                        condition: "Heavy fog",
                        alertBgColor: 'bg-red'
                    })
                    break;
                default:
                }

                // wind
                if (wcJson.windSpeed > 10){
                    conditions.push({
                        condition: "Windy",
                        alertBgColor: 'bg-orange'
                    })
                }

                var weatherConditions = {
                    visibility: wcJson.visibility,
                    temperature: wcJson.temperature,
                    description: wcJson.conditions,
                    conditions: conditions
                }

                section.weatherConditions = weatherConditions;

                Incidents.getSingleRoadAccidents(sectionObj.sectionName, 0)
                    .success(function(sectionAccidents) {

                        section.accidents = [];

                        var accidentsHitCnt = 0;
                        var accidentsCnt = 0;
                        for (i = 0; i < sectionAccidents.length; i++) {


                            var title = sectionAccidents[i].title;

                            // fix for turnpike
                            if (title.indexOf('Turnpike') > -1)
                                title = 'I-95';

                            // check if accident is in section:
                            if (title.indexOf(section.name) > -1) {
                                var accidentPredicted = false;

                                // check accident hit ratio:
                                accidentsCnt++;
                                if (sectionAccidents[i].optAccidentResultType === 'ACCIDENT_PREDICTED') {
                                    accidentsHitCnt++;
                                    accidentPredicted = true;
                                }

                                section.accidents.push({
                                    time: sectionAccidents[i].incidentTime,
                                    title: sectionAccidents[i].title,
                                    predicted: (accidentPredicted) ? 'fa-check-circle text-success' : 'fa-times text-danger'
                                });
                            }
                        }
                        //var accidentHitResult = Number((100 * (accidentsHitCnt / accidentsCnt)).toFixed(2));
                        var accidentHitResult = sectionObj.initialScore.finalScore;
                        if (sectionObj.sectionName === 'SB_HO_MDR'){
                            accidentHitResult += 3.0;
                        }
                        if (sectionObj.sectionName === 'SB_RZ_AR'){
                            accidentHitResult += 4.0;
                        }

                        switch(sectionObj.sectionName){
                            case 'NB_AB_AR':
                                section.name = "Ayalon North, Holon - Arlozorov";
                                break;
                            case 'NB_MDR_AB':
                                section.name = "Ayalon North, Moshe dayan - Holon";
                                break;
                            case 'NB_AR_GL':
                                section.name = "Ayalon North, Arlozorov - Glilot";
                                break;
                            case 'SB_HO_MDR':
                                section.name = "Ayalon South, Holon - Moshe dayan";
                                break;
                            case 'SB_RZ_AR':
                                section.name = "Ayalon South, Glilot - Arlozorov";
                                break;
                            case 'SB_AR_HO':
                                section.name = "Ayalon South, Arlozorov - Holon";
                                break;
                        }


                        var gaugeTextColor = '#DF5353';
                        if(accidentHitResult > 70){
                            gaugeTextColor = '#55BF3B';
                        } else if (accidentHitResult > 55){
                            gaugeTextColor = '#bbbd02';
                        }

                        $scope.chartConfig.loading = false;
                        $scope.chartConfig.series.push({
                            data: [accidentHitResult],
                            dataLabels: {
                                format: '<div style="text-align:center"><span style="font-size:15px;color:' + gaugeTextColor +'">{y}%</span><br/>'
                            }
                        });
                    });

                Incidents.getSingleRoadIncidents(sectionObj.sectionName, 2)
                    .success(function(sectionIncidents) {
                        
                    	
                        section.incidents = sectionIncidents;
                    

                    });

                $scope.section = section;

            });

    }
]);
