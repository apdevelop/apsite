// public/js/controllers/currentCtrl.js
var currentCtrl = angular.module('currentCtrl',[]);
currentCtrl.controller('currentCtrl',[ '$scope', '$location', 'Sections', 'Predictions', function($scope, $location, Sections, Prediction){

		$scope.isActive = function(viewLocation){
			
			var result = ($location.path().indexOf(viewLocation) > -1);
			return result;
		}

		// ------------------------------ //
		// -- Section dataBase handler -- //
		// ------------------------------ //
		Sections.getOverview().then(function(data){
			$scope.sections = data.sections;
		})

}]);