// app/models/engines.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var EngineCfg = require('./engineCfgs.js');
var State = require('./states.js');
var Score = require('./scores.js');

var Engine = new Schema({
	crTime : Date,
	sectionName : String,
	engineName : String,
	engineType : String,
	lastUpdated : Date,
	engineCfg : EngineCfg.schema,
	state : State.schema,
	states : [State.schema],
	initialScore : Score.schema,
	score : Score.schema,
	scores : [Score.schema]
});

module.exports = mongoose.model('Engine', Engine);