// app/models/engineCfgs.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var EngineCfg = new Schema({
	engineResultTH : Number,
	networkUrl : String
});

module.exports = mongoose.model('EngineCfg', EngineCfg);