// app/models/scores.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var Score = new Schema({
	crTime : Date,
	finalScore : Number,
	accidentsScore : Number,
	nonAccidentsScore : Number,
	accidentsHit : Number,
	accidentsMiss : Number,
	nonAccidentsHit : Number,
	nonAccidentsMiss : Number
});
module.exports = mongoose.model('Score', Score);
