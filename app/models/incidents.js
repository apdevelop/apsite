// app/models/incidents.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var LatLng = require('./latLngs.js');

// Incidents collection: 
var Incident = new Schema({
    crTime		: Date,
    incidentTime: Date,
    title    	: String,
    type 		: String,
    description	: String,
    location	: LatLng.schema,
    incidentCode: String,
    optSection	: String,
    optAccidentResultType : String
},{
	collection : 'Incidents'
});

module.exports = mongoose.model('Incident', Incident);