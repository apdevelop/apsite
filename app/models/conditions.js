// app/models/conditions.js
// weather conditions format numbers

var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var Conditions = new Schema({
	conditions: String,
	temperature: Number,
	visibility: Number,
	windSpeed: Number,
	windDirection: Number,
	rain: Number,
	snow: Number,
	fog: Number
})
module.exports = mongoose.model('Conditions', Conditions);