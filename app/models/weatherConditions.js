// app/models/weatherConditions.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var Conditions = require('./conditions.js');

var WeatherCondition = new Schema({
	crTime : Date,
	type : String,
	conditions : Conditions.schema
});
module.exports = mongoose.model('WeatherCondition', WeatherCondition);
