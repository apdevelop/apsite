// app/models/predictionSections.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var LocationDescription = require('./locationDescriptions.js');
var Prediction = require('./prediction.js');


var PredictionSection = new Schema({
	crTime: Date,
	sectionName: String,
	decoratedName: String,
	sectionType: String,
	locationDescription: LocationDescription.schema,
	prediction: [Prediction.schema]
},
{
	collection : 'SectionsPrediction'
});

module.exports = mongoose.model('PredictionSection', PredictionSection);