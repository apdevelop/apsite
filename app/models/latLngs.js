// app/models/latLngs.js
// TODO: change to geospacial mongoose
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var LatLng = new Schema({
	lat : Number,
	lng : Number
});
module.exports = mongoose.model('LatLng', LatLng);