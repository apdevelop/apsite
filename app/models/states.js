// app/models/states.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var Incident = require("./incidents");

var State = new Schema({
	crTime : Date,
	state : String,
	inputCode : String,
	inputVector : String,
	incidents : [Incident.schema]
});
module.exports = mongoose.model('State', State);