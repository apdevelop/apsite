// app/models/sections.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var LocationDescription = require('./locationDescriptions.js');
var SectionCfg = require('./sectionCfgs.js');
var WeatherCondition = require('./weatherConditions.js');
var Engine = require('./engines.js');
var State = require('./states.js');
var Score = require('./scores.js');
var Incident = require('./incidents.js');

var Section = new Schema({
	crTime : Date,
	sectionName : String,
	decoratedName: String,
	sectionType : String,
	locationDescription : LocationDescription.schema, 
	lastUpdated : Date,
	sectionCfg : SectionCfg.schema,
	weatherConditions : WeatherCondition.schema,
	engines : [Engine.schema],
	state : State.schema,
	initialScore : Score.schema,
	score : [Score.schema],
	incidents : Incident.schema
},
{
	collection : 'Sections'
});

module.exports = mongoose.model('Section', Section);