// app/models/locations.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;
var LatLng = require('./latLngs.js');

var LocationDescription = new Schema({
	route : String,
	mile : Number,
	length : Number,
	closestTown : String,
	state : String,
	majorWeatherStation : String,
	startLoc : LatLng.schema,
	endLoc : LatLng.schema,
	routeDescription : [LatLng.schema]
});
module.exports = mongoose.model('LocationDescription', LocationDescription);