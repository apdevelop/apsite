// app/models/sections.js
var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var WeatherConditions = require('./weatherConditions.js');
var State = require('./states.js');

var Prediction = new Schema({
	time: Date,
	predictionIndex: Number,
	State: State.schema,
	weatherConditions: WeatherConditions.schema
});

module.exports = mongoose.model('Prediction', Prediction);