// app/routes.js
var Incident = require('./models/incidents');
var Section = require('./models/sections');
var PredictionSection = require('./models/predictionSections')
var ObjectId = require('mongoose').Schema.ObjectId;

module.exports = function(app){

	// backend routes:
	// ---------------

	// get all incidents /api/incidents
	app.get('/api/incidents', function(req,res){
		Incident.find().exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}
			res.json(incidents);
		})
	})

	// get all incidents by type /api/incidents/:incident_type
	app.get('/api/incidents/:incident_type', function(req,res){
		Incident.find( {type :req.params.incident_type} ).sort({_id: -1}).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}

			res.json(incidents);
		})
	});

	// get all incidents by type & section /api/incidents/:incident_type
	app.get('/api/incidents/:incident_type/:section', function(req,res){
		Incident.find({ type:req.params.incident_type, optSection:req.params.section }).sort({_id: -1}).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}

			res.json(incidents);
		})
	});


	// get all incidents by type /api/incidents/:incident_type
	app.get('/api/incidents_all/:incident_type', function(req,res){
		Incident.find( {type :req.params.incident_type} ).sort({_id: -1}).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}

			res.json(incidents);
		})
	});

	// get all incidents by type & section /api/incidents/:incident_type
	app.get('/api/incidents_all/:incident_type/:num_of_incidents', function(req,res){
		Incident.find({ type:req.params.incident_type }).sort({_id: -1}).limit(req.params.num_of_incidents).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}

			res.json(incidents);
		})
	});



	// get incidents by type and number of incidents /api/incidents/:incident_type:/num_of_incidents
	app.get('/api/incidentsG/:incident_type/:num_of_incidents', function(req,res){
		Incident.find( {type :req.params.incident_type} ).sort({_id: -1}).limit(req.params.num_of_incidents).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}
			res.json(incidents);
		})
	});

	// get last #num_of_incidents incidents by type & section /api/incidents/:incident_type:/num_of_incidents
	app.get('/api/incidents/:incident_type/:section/:num_of_incidents', function(req,res){
		Incident.find({ type :req.params.incident_type, optSection:req.params.section }).sort({_id: -1}).limit(req.params.num_of_incidents).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}
			res.json(incidents);
		})
	});

	// get all accidents with optSection differ than NONE
	app.get('/api/accidentsInRange', function(req,res){
		Incident.find({ type : 'ACCIDENT', optSection: {'$ne' : "NONE"} }).exec(function(err,accidents){
			if (err){
				res.send(err);
				return;
			}
			res.json(accidents);
		})
	})

	// get last # accidents with optSection differ than NONE
	app.get('/api/accidentsInRange/:num_of_accidents', function(req,res){
		Incident.find({ type : 'ACCIDENT', optSection: {'$ne' : "NONE"} }).sort({_id: -1}).limit(req.params.num_of_accidents).exec(function(err,accidents){
			if (err){
				res.send(err);
				return;
			}
			res.json(accidents);
		})
	})
/*
	TODO: implement, solve http get request with ?&
	// get incidents in the past #Hours
	app.get('/api/incidents/:incident_type/:section/:num_of_incidents/:hours', function(req,res){
		Incident.find({ _id:{ $gt: ObjectId.createFromTimestamp(Date.now()*1000 - req.params.hours*60*60)},
						type :req.params.incident_type, optSection:req.params.section 
			}).sort({_id: -1}).limit(req.params.num_of_incidents).exec(function(err, incidents){
			if (err){
				res.send(err);
				return;
			}
			res.json(incidents);
		})
	});
*/




	// post incident /api/incidents


	// -------------------- //
	// -- Sections routes -- //
	// -------------------- //

	// get all engines:
	app.get('/api/sections', function(req,res){
		Section.find().exec(function(err, sections){
			if (err){
				res.send(err);
				return;
			}
			res.json(sections);
		})
	})

	// get all sections in route
	app.get('/api/sections/:section_name', function(req,res){
		Section.find( {sectionName : { $regex: new RegExp(req.params.section_name + '*'), $options:'si' } } ).exec(function(err,sections){
			if (err){
				res.send(err);
				return;
			}
			res.json(sections);
		})
	})

	// get single section
	app.get('/api/section/:section_name', function(req,res){
		Section.find( {sectionName : req.params.section_name } ).exec(function(err,sections){
			if (err){
				res.send(err);
				return;
			}
			res.json(sections);
		})
	})


	// ----------------------- //
	// -- prediction routes -- //
	// ----------------------- //

	// get all prediction:
	app.get('/api/predictions', function(req,res){
		PredictionSection.find().exec(function(err, predictions){
			if (err){
				res.send(err);
				return;
			}
			res.json(predictions);
		})
	})

	// get single section prediction:
	app.get('/api/predictions/:section_name', function(req,res){
		PredictionSection.find( {sectionName: req.params.section_name} ).exec(function(err, predictions){
			if (err){
				res.send(err);
				return;
			}
			res.json(predictions);
		})
	})



	// frontend routes:
	app.get('*', function(req,res){
		res.sendFile('index.html', {'root': __dirname + "/../public/" });
	})

}